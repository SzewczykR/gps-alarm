package com.example.budzikgps

import android.app.*
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.location.Location
import android.os.*
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.google.android.gms.location.*
import org.greenrobot.eventbus.EventBus


class TrackingService : Service() {

    inner class LocalBinder : Binder() {
        internal val service: TrackingService
            get() = this@TrackingService
    }

    private val channelId = "channel_99"
    private val extraStartedFromNotification = "$packageName.started_from_notification"
    private var updateIntervalInMil: Long = 5000
    private var fastedUpdateIntervalInMil: Long = 5000
    private val notificationId = 1234
    private val mBinder = LocalBinder()
    private var mChangingConfiguration = false
    private var mNotificationManager: NotificationManager? = null
    private var locationRequest: LocationRequest? = null
    private var fusedLocationProviderClient: FusedLocationProviderClient? = null
    private var locationCallback: LocationCallback? = null
    private var mServiceHandler: Handler? = null
    private var mLocation: Location? = null
    private var radius = 200
    private var gracePeriod = 120

    private val notification: Notification
        get() {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("EXTRA_STARTED_FROM_TRACKING_SERVICE", true)
            val title = Common.getNotificationTitle(this, mLocation)
            intent.putExtra(extraStartedFromNotification, true)
            val activityPendingIntent =
                PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            val builder = NotificationCompat.Builder(this)
                .addAction(R.drawable.ic_cancel_black_24dp, "Cancel", activityPendingIntent)
                .setContentTitle(title)
                .setOngoing(false)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setWhen(System.currentTimeMillis())
                .setSound(null)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                builder.setChannelId(channelId)
            return builder.build()
        }

    override fun onBind(intent: Intent): IBinder? {
        stopForeground(true)
        mChangingConfiguration = false
        return mBinder
    }

    override fun onRebind(intent: Intent?) {
        stopForeground(true)
        mChangingConfiguration = false
        super.onRebind(intent)
    }

    override fun onUnbind(intent: Intent?): Boolean {
        if (!mChangingConfiguration && Common.requestingLocationUpdates(this))
            startForeground(notificationId, notification)
        return true
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        mChangingConfiguration = true
    }

    override fun onCreate() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                super.onLocationResult(p0)
                onNewLocation(p0!!.lastLocation)
            }
        }
        val config = Config(this)
        radius = config.getRadius()
        gracePeriod = config.getGracePeriod()
        updateIntervalInMil = config.getFrequency().toLong() * 1000
        fastedUpdateIntervalInMil = updateIntervalInMil
        createLocationRequest()
        getLastLocation()
        val handlerThread = HandlerThread("GPSAlarmService")
        handlerThread.start()
        mServiceHandler = Handler(handlerThread.looper)
        mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = packageName
            val mChannel =
                NotificationChannel(channelId, name, NotificationManager.IMPORTANCE_LOW)
            mNotificationManager!!.createNotificationChannel(mChannel)
        }
    }

    private fun getLastLocation() {
        try {
            fusedLocationProviderClient!!.lastLocation
                .addOnCompleteListener { task ->
                    if (task.isSuccessful && task.result != null)
                        mLocation = task.result
                }
        } catch (ex: SecurityException) {
        }
    }

    private fun createLocationRequest() {
        val config = Config(this)
        updateIntervalInMil = config.getFrequency().toLong() * 1000
        fastedUpdateIntervalInMil = updateIntervalInMil
        radius = config.getRadius()
        gracePeriod = config.getGracePeriod()
        locationRequest = LocationRequest()
        locationRequest!!.interval = updateIntervalInMil
        locationRequest!!.fastestInterval = fastedUpdateIntervalInMil
        locationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun onNewLocation(lastLocation: Location?) {
        mLocation = lastLocation!!
        if (Common.alarmCheck(mLocation, radius)) {
            Toast.makeText(this, "ALARM", Toast.LENGTH_LONG).show()
            val intent = Intent(this, AlarmService::class.java)
            startService(intent)
            removeLocationUpdates()
            stopSelf()
        } else {
            EventBus.getDefault().postSticky(BackgroundLocation(mLocation!!))
            if (serviceIsRunningInForeground(this))
                mNotificationManager!!.notify(notificationId, notification)
        }
    }

    private fun serviceIsRunningInForeground(context: Context): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (javaClass.name==(service.service.className))
                if (service.foreground)
                    return true
        }
        return false
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val startedFromNotification =
            intent!!.getBooleanExtra("EXTRA_STARTED_FROM_MAPS", false)
        if (startedFromNotification) {
            removeLocationUpdates()
            stopSelf()
        }
        return START_NOT_STICKY
    }

    fun removeLocationUpdates() {
        try {
            fusedLocationProviderClient!!.removeLocationUpdates(locationCallback!!)
            Common.setRequestingLocationUpdates(this, false)
            stopSelf()
        } catch (ex: SecurityException) {
            Common.setRequestingLocationUpdates(this, true)
        }
    }

    override fun onDestroy() {
        mServiceHandler!!.removeCallbacksAndMessages(null)
        super.onDestroy()
    }


    fun requestLocationUpdates() {
        Common.setRequestingLocationUpdates(this, true)
        startService(Intent(applicationContext, TrackingService::class.java))
        try {
            fusedLocationProviderClient!!.requestLocationUpdates(
                locationRequest!!, locationCallback!!,
                Looper.myLooper()
            )
        } catch (ex: SecurityException) {
            Common.setRequestingLocationUpdates(this, false)
        }
    }
}
