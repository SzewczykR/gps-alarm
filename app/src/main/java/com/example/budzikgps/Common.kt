package com.example.budzikgps

import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.preference.PreferenceManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.livinglifetechway.quickpermissions_kotlin.util.QuickPermissionsOptions
import com.livinglifetechway.quickpermissions_kotlin.util.QuickPermissionsRequest
import java.io.IOException
import java.lang.reflect.InvocationTargetException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

object Common {

    const val KEY_REQUEST_LOCATION_UPDATE = "requesting_location_update"
    var destination: LatLng? = null

    fun getLocationByAddress(context: Context, location: String): LatLng? {
        var addressList: List<Address>? = null
        if (location == "null" || location == "") {
            Toast.makeText(context, "Podaj adres.", Toast.LENGTH_LONG).show()
            return null
        } else {
            val geocoder = Geocoder(context)
            try {
                addressList = geocoder.getFromLocationName(location, 1)
            } catch (e: IOException) {
                e.printStackTrace()
            }

            var address: Address? = null

            try {
                address = addressList!![0]
            } catch (e: IndexOutOfBoundsException) {
                Toast.makeText(context, "Nie znaleziono adresu.", Toast.LENGTH_LONG).show()
                return null
            }

            return LatLng(address.latitude, address.longitude)

        }


    }

    fun getAddressByLocation(context: Context, location: LatLng): String? {
        val geocoder = Geocoder(context)
        val addresses: List<Address>

        try {
            addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1)
        } catch (e: InvocationTargetException) {
            return null
        }

        var address = addresses[0].getAddressLine(0).toString()
        if (address.contains("Unnamed Road")) address =
            address.replace("Unnamed Road", "Nieznana droga")
        return address
    }

    @SuppressLint("SimpleDateFormat")
    fun getTimestamp(): String {
        val date = Date()
        val formatter = SimpleDateFormat("HH:mm:ss, dd-MM-yyyy ")
        return formatter.format(date).toString()
    }

    fun getQuickPermissionsOptions(context: Context): QuickPermissionsOptions {
        return QuickPermissionsOptions(
            rationaleMethod = { req -> rationaleCallback(req, context) },
            permanentDeniedMethod = { req -> permissionsPermanentlyDenied(req, context) }
        )
    }

    private fun rationaleCallback(req: QuickPermissionsRequest, context: Context) {
        AlertDialog.Builder(context)
            .setTitle("Uprawnienia")
            .setMessage("Do działania aplikacji niezbędne jest uprawnienie do sprawdzania lokalizacji")
            .setNegativeButton("anuluj") { dialogInterface, i ->
                dialogInterface.dismiss()
                req.cancel()
            }
            .setPositiveButton("ok") { dialogInterface, i ->
                dialogInterface.dismiss()
                req.proceed()
            }
            .show()
    }

    private fun permissionsPermanentlyDenied(req: QuickPermissionsRequest, context: Context) {
        AlertDialog.Builder(context)
            .setTitle("Uprawnienia")
            .setMessage("Do działania aplikacji niezbędne jest uprawnienie do sprawdzania lokalizacji")
            .setNegativeButton("anuluj") { dialogInterface, i ->
                dialogInterface.dismiss()
                req.cancel()
            }
            .setPositiveButton("ustawienia") { dialogInterface, i ->
                dialogInterface.dismiss()
                req.openAppSettings()
            }
            .show()
    }

    fun prepareHistory(historyEntry: HistoryEntry, context: Context) {
        val config = Config(context)
        var history: Set<String> = config.getHistory()
        if (history.toString() == "" || history.toString() == "[]") {

            val list = listOf(historyEntry.toString())
            history = list.toSet()
            config.setHistory(history)
        } else {

            val list: MutableList<String> = history.toMutableList()
            list.add(historyEntry.toString())
            history = list.toSet()
            config.setHistory(history)
        }


    }

    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                when {
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {

                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {

                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {

                        return true
                    }
                }
            }
        }
        return false
    }

    fun pushHistoryEntryToFB(historyEntry: HistoryEntry) {
        val db = FirebaseFirestore.getInstance()
        val uid = FirebaseAuth.getInstance().uid ?: ""

        if (uid != "") {

            db.collection("history").document(uid).collection("history").get()
                .addOnSuccessListener {
                    val index = it.size().toString()


                    val entry = hashMapOf(
                        "latitude" to historyEntry.location.latitude,
                        "longitude" to historyEntry.location.longitude,
                        "address" to historyEntry.address,
                        "favorite" to historyEntry.favorite.toString(),
                        "timestamp" to historyEntry.timestamp
                    )

                    db.collection("history").document(uid).collection("history").document(index)
                        .set(entry)
                }
        }
    }

    fun alarmCheck(location: Location?, radius: Int): Boolean {
        if (destination != null && location != null) {
            val distance = calculateDistance(
                location.latitude,
                location.longitude,
                destination!!.latitude,
                destination!!.longitude
            )
            return distance <= radius
        }
        return false
    }

    fun getLocationLatLng(location: Location?): LatLng? {
        return if (location != null) {
            LatLng(location.latitude, location.longitude)
        } else null
    }

    fun distanceToText(distance: Int): String {
        return if (distance < 1000) {
            distance.toString() + "m"
        } else {
            String.format("%.1f km", distance.toDouble() / 1000)
        }
    }

    fun calculateDistance(
        startLat: Double,
        startLng: Double,
        destLat: Double,
        destLng: Double
    ): Int {
        val earthRadius = 6371000
        val dLat = Math.toRadians(destLat - startLat)
        val dLng = Math.toRadians(destLng - startLng)
        val a = sin(dLat / 2) * sin(dLat / 2) +
                cos(Math.toRadians(startLat)) * cos(Math.toRadians(destLat)) *
                sin(dLng / 2) * sin(dLng / 2)
        val c: Double = 2 * atan2(sqrt(a), sqrt(1 - a))

        return (earthRadius * c).toInt()
    }

    fun getNotificationTitle(context: Context, location: Location?): String {
        return if (location != null && destination != null) {
            val distance = calculateDistance(
                location.latitude,
                location.longitude,
                destination!!.latitude,
                destination!!.longitude
            )
            val distanceToText = distanceToText(distance)
            "Odległość od celu: $distanceToText"

        } else {
            "Nie udało się określić odległości od celu."
        }
    }

    fun setRequestingLocationUpdates(context: Context, value: Boolean) {
        PreferenceManager.getDefaultSharedPreferences(context)
            .edit()
            .putBoolean(KEY_REQUEST_LOCATION_UPDATE, value)
            .apply()
    }

    fun requestingLocationUpdates(context: Context): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(context)
            .getBoolean(KEY_REQUEST_LOCATION_UPDATE, false)
    }

    fun statusCheck(context: Context): Boolean {
        val manager =
            context.getSystemService(LOCATION_SERVICE) as LocationManager
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }


}