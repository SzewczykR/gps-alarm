package com.example.budzikgps

import android.Manifest
import android.content.*
import android.location.Location
import android.os.Bundle
import android.os.IBinder
import android.preference.PreferenceManager
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.budzikgps.Common.getAddressByLocation
import com.example.budzikgps.Common.getLocationByAddress
import com.example.budzikgps.Common.getQuickPermissionsOptions
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.annotations.Marker
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import kotlinx.android.synthetic.main.activity_mapbox.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*


class MapboxActivity : AppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener {

    private var mapView: MapView? = null
    private var currentLocationMarker: Marker? = null
    private var destinationLocationMarker: Marker? = null
    private var mService: TrackingService? = null
    private var mBound = false
    private var destination: LatLng? = null
    private var currentLocation: Location? = null
    private var fusedLocationProviderClient: FusedLocationProviderClient? = null
    private var mapBoxMap: MapboxMap? = null
    private var startedFromHistory = false
    private val quickPermissionsOptions = getQuickPermissionsOptions(this)
    private val mServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            mService = null
            mBound = false
        }

        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            val binder = p1 as TrackingService.LocalBinder
            mService = binder.service
            mBound = true

        }

    }
    private var checkConnection: CheckConnection? = null

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onBackgroundLocationRetrieve(event: BackgroundLocation) {
        if (event.location != null) {
            val distance = Common.calculateDistance(
                event.location.latitude,
                event.location.longitude,
                destination!!.latitude,
                destination!!.longitude
            )
            val distanceText = Common.distanceToText(distance)
            Toast.makeText(this, "Odległość od celu: $distanceText", Toast.LENGTH_LONG).show()
            if (currentLocationMarker != null)
                currentLocationMarker!!.remove()

            mapView?.getMapAsync { mapboxMap ->
                val retrievedLocation = Common.getLocationLatLng(event.location)!!
                currentLocationMarker = mapboxMap.addMarker(
                    MarkerOptions().position(
                        LatLng(
                            retrievedLocation.latitude,
                            retrievedLocation.longitude
                        )
                    )
                )
            }

        } else {
            Toast.makeText(this, "Nie udało się pobrać lokalizacji (null)", Toast.LENGTH_LONG)
                .show()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mapbox.getInstance(applicationContext, getString(R.string.mapbox_access_token))
        setContentView(R.layout.activity_mapbox)

        startedFromHistory = intent.getBooleanExtra("STARTED_FROM_HISTORY", false)

        if (startedFromHistory) {
            val latitudeFromHistory = intent.getDoubleExtra("LATITUDE", 0.0)
            val longitudeFromHistory = intent.getDoubleExtra("LONGITUDE", 0.0)

            destination = LatLng(latitudeFromHistory, longitudeFromHistory)
        }

        val notif = intent.getBooleanExtra("EXTRA_STARTED_FROM_ALARM_SERVICE", false)
        if (notif) {

            val intent = Intent(this, AlarmService::class.java)
            intent.putExtra("EXTRA_STARTED_FROM_MAPS", true)
            startService(intent)
        }

        val notif2 = intent.getBooleanExtra("EXTRA_STARTED_FROM_TRACKING_SERVICE", false)
        if (notif2) {
            val intent = Intent(this, TrackingService::class.java)
            intent.putExtra("EXTRA_STARTED_FROM_MAPS", true)
            startService(intent)
        }

        mapView = findViewById(R.id.mapView)
        mapView?.onCreate(savedInstanceState)
        mapView?.getMapAsync { mapboxMap ->
            mapBoxMap = mapboxMap
            mapboxMap.setStyle(Style.MAPBOX_STREETS) {
                getLastLocation()
                mapboxMap.moveCamera(CameraUpdateFactory.zoomTo(14.00))
                if (startedFromHistory) {
                    if (destinationLocationMarker != null)
                        destinationLocationMarker!!.remove()
                    destinationLocationMarker =
                        mapboxMap.addMarker(MarkerOptions().position(destination).title("Cel"))
                    mapboxMap.moveCamera(CameraUpdateFactory.newLatLng(destination!!))
                }
                mapboxMap.addOnMapClickListener { point ->
                    destination = point
                    if (destinationLocationMarker != null)
                        destinationLocationMarker!!.remove()
                    destinationLocationMarker =
                        mapboxMap.addMarker(MarkerOptions().position(point).title("Destination"))

                    mapboxMap.moveCamera(CameraUpdateFactory.newLatLng(point))

                    true
                }
            }
        }

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        mapbox_searchaddress_btn.setOnClickListener {
            searchLocation()
        }

        mapbox_startalarm_btn.setOnClickListener {
            val config = Config(this)

            if (Common.isOnline(this)) {

                runWithPermissions(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,

                    options = quickPermissionsOptions
                ) {
                    getLastLocation()
                    if (destination != null) {
                        Common.destination = com.google.android.gms.maps.model.LatLng(
                            destination!!.latitude,
                            destination!!.longitude
                        )
                        mService!!.requestLocationUpdates()


                        val timer = Timer()
                        val period = config.getGracePeriod().toLong() * 1000
                        checkConnection = CheckConnection(this)

                        if (period != 0L)
                            timer.schedule(checkConnection, period, period)

                        setButtonState(true)
                        val timestamp = Common.getTimestamp()


                        val latitude: Double = destination!!.latitude
                        val longitude: Double = destination!!.longitude
                        val locationGmaps =
                            com.google.android.gms.maps.model.LatLng(latitude, longitude)
                        val address = if (getAddressByLocation(this, locationGmaps) == null) {
                            "Nie odnaleziono adresu"
                        } else {
                            getAddressByLocation(this, locationGmaps)
                        }

                        val historyEntry = HistoryEntry(
                            locationGmaps,
                            address!!,
                            false,
                            timestamp
                        )

                        Common.prepareHistory(historyEntry, this)
                        Common.pushHistoryEntryToFB(historyEntry)
                    } else {
                        Toast.makeText(
                            this,
                            "Nie wybrałeś lokalizacji docelowej!",
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }

                }
            } else {
                Toast.makeText(this, "Brak połączenia z internetem.", Toast.LENGTH_LONG).show()
            }

        }

        mapbox_stopalarm_btn.setOnClickListener {
            mService!!.removeLocationUpdates()
            checkConnection?.disableChecking()
            setButtonState(false)
        }

        setButtonState(Common.requestingLocationUpdates(this@MapboxActivity))
        bindService(
            Intent(this@MapboxActivity, TrackingService::class.java),
            mServiceConnection,
            Context.BIND_AUTO_CREATE
        )

        setButtonState(false)

    }

    private fun buildAlertMessageNoGps() {
        val builder =
            AlertDialog.Builder(this)
        builder.setMessage("Usługa lokalizacji w twoim urządzeniu jest wyłączona. Czy chcesz ją włączyć?")
            .setCancelable(false)
            .setPositiveButton("TAK"
            ) { dialog, id -> startActivityForResult(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0) }
            .setNegativeButton("NIE"
            ) { dialog, id -> dialog.cancel() }
        val alert = builder.create()
        alert.show()
    }

    private fun setButtonState(boolean: Boolean) {
        if (boolean) {
            mapbox_stopalarm_btn.isEnabled = true
            mapbox_startalarm_btn.isEnabled = false
        } else {
            mapbox_stopalarm_btn.isEnabled = false
            mapbox_startalarm_btn.isEnabled = true
        }
    }

    private fun getLastLocation() {
        runWithPermissions(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            options = quickPermissionsOptions
        ) {
            try {
                fusedLocationProviderClient!!.lastLocation
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful && task.result != null) {
                            currentLocation = task.result
                            if (currentLocationMarker != null)
                                currentLocationMarker!!.remove()

                            if (currentLocation != null) {
                                val currentLocationLatLng =
                                    com.google.android.gms.maps.model.LatLng(
                                        currentLocation!!.latitude,
                                        currentLocation!!.longitude
                                    )

                                val temp = LatLng(
                                    currentLocationLatLng.latitude,
                                    currentLocationLatLng.longitude
                                )
                                currentLocationMarker =
                                    mapBoxMap!!.addMarker(
                                        MarkerOptions().position(temp)
                                            .title("Twoja lokalizacja!")
                                    )
                                mapBoxMap!!.moveCamera(CameraUpdateFactory.newLatLng(temp))
                            }

                        } else
                            Toast.makeText(
                                this,
                                "Nie udało się pobrać ostatniej znanej lokalizacji. Włącz usługę lokalizacji. Gdy uruchomisz alarm nastąpi ponowna próba pobrania lokalizacji.",
                                Toast.LENGTH_LONG
                            ).show()

                        if (!Common.statusCheck(this)) buildAlertMessageNoGps()
                    }
            } catch (ex: SecurityException) {
                Log.e("catch", "" + ex.message)
            }
        }
    }

    private fun searchLocation() {
        val location = mapbox_searchaddress_txt.text.toString()
        if (getLocationByAddress(this, location) != null) {

            val temp = getLocationByAddress(this, location)

            destination = LatLng(temp!!.latitude, temp.longitude)
            if (destinationLocationMarker != null)
                destinationLocationMarker!!.remove()
            destinationLocationMarker =

                mapBoxMap!!.addMarker(MarkerOptions().position(destination).title("Cel"))
            mapBoxMap!!.moveCamera(CameraUpdateFactory.newLatLng(destination!!))

        }
    }

    override fun onStart() {
        super.onStart()
        mapView?.onStart()
        PreferenceManager.getDefaultSharedPreferences(this)
            .registerOnSharedPreferenceChangeListener(this)
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        PreferenceManager.getDefaultSharedPreferences(this)
            .unregisterOnSharedPreferenceChangeListener(this)
        EventBus.getDefault().unregister(this)
        super.onStop()
        mapView?.onStop()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView?.onPause()
    }

    override fun onSharedPreferenceChanged(p0: SharedPreferences?, p1: String?) {
        if (p1.equals(Common.KEY_REQUEST_LOCATION_UPDATE))
            setButtonState(p0!!.getBoolean(Common.KEY_REQUEST_LOCATION_UPDATE, false))
    }

}