package com.example.budzikgps

import android.Manifest
import android.content.*
import android.location.Location
import android.os.Bundle
import android.os.IBinder
import android.preference.PreferenceManager
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.budzikgps.Common.getAddressByLocation
import com.example.budzikgps.Common.getLocationByAddress
import com.example.budzikgps.Common.getQuickPermissionsOptions
import com.example.budzikgps.Common.getTimestamp
import com.example.budzikgps.Common.isOnline
import com.example.budzikgps.Common.prepareHistory
import com.example.budzikgps.Common.pushHistoryEntryToFB
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_maps.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*


class MapsActivity : AppCompatActivity(), OnMapReadyCallback,
    SharedPreferences.OnSharedPreferenceChangeListener {

    private lateinit var mMap: GoogleMap
    private var currentLocationMarker: Marker? = null
    private var destinationLocationMarker: Marker? = null
    private var mService: TrackingService? = null
    private var mBound = false
    private var destination: LatLng? = null
    private var currentLocation: Location? = null
    private var fusedLocationProviderClient: FusedLocationProviderClient? = null
    private var startedFromHistory = false
    private val quickPermissionsOptions = getQuickPermissionsOptions(this)
    private val mServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            mService = null
            mBound = false
        }

        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            val binder = p1 as TrackingService.LocalBinder
            mService = binder.service
            mBound = true

        }

    }
    private var checkConnection: CheckConnection? = null

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onBackgroundLocationRetrieve(event: BackgroundLocation) {
        if (event.location != null) {
            val distance = Common.calculateDistance(
                event.location.latitude,
                event.location.longitude,
                destination!!.latitude,
                destination!!.longitude
            )
            val distanceText = Common.distanceToText(distance)
            Toast.makeText(this, "Odległość od celu: $distanceText", Toast.LENGTH_LONG).show()
            if (currentLocationMarker != null)
                currentLocationMarker!!.remove()

            currentLocationMarker =
                mMap.addMarker(MarkerOptions().position(Common.getLocationLatLng(event.location)!!))
        } else {
            Toast.makeText(this, "Nie udało się pobrać lokalizacji (null)", Toast.LENGTH_LONG)
                .show()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        startedFromHistory = intent.getBooleanExtra("STARTED_FROM_HISTORY", false)

        if (startedFromHistory) {
            val latitudeFromHistory = intent.getDoubleExtra("LATITUDE", 0.0)
            val longitudeFromHistory = intent.getDoubleExtra("LONGITUDE", 0.0)

            destination = LatLng(latitudeFromHistory, longitudeFromHistory)
        }

        val notif = intent.getBooleanExtra("EXTRA_STARTED_FROM_ALARM_SERVICE", false)
        if (notif) {

            val intent = Intent(this, AlarmService::class.java)
            intent.putExtra("EXTRA_STARTED_FROM_MAPS", true)
            startService(intent)
        }

        val notif2 = intent.getBooleanExtra("EXTRA_STARTED_FROM_TRACKING_SERVICE", false)
        if (notif2) {
            val intent = Intent(this, TrackingService::class.java)
            intent.putExtra("EXTRA_STARTED_FROM_MAPS", true)
            startService(intent)
        }

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        maps_search_address_btn.setOnClickListener {
            searchLocation()
        }

        service_start_button.setOnClickListener {

            val config = Config(this)
            if (isOnline(this)) {
                runWithPermissions(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    options = quickPermissionsOptions
                ) {
                    getLastLocation()
                    if (destination != null) {
                        Common.destination = destination
                        mService!!.requestLocationUpdates()
                        val timer = Timer()
                        val period = config.getGracePeriod().toLong() * 1000
                        checkConnection = CheckConnection(this)
                        if (period != 0L)
                            timer.schedule(checkConnection, period, period)
                        setButtonState(true)
                        val timestamp = getTimestamp()

                        val address = if (getAddressByLocation(this, destination!!) == null) {
                            "Nie odnaleziono adresu"
                        } else {
                            getAddressByLocation(this, destination!!)
                        }
                        val historyEntry = HistoryEntry(
                            destination!!,
                            address!!,
                            false,
                            timestamp
                        )

                        prepareHistory(historyEntry, this)
                        pushHistoryEntryToFB(historyEntry)

                    } else {
                        Toast.makeText(
                            this,
                            "Nie wybrałeś lokalizacji docelowej.",
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }
                }

            } else {
                Toast.makeText(this, "Brak połączenia z internetem.", Toast.LENGTH_LONG).show()
            }
        }

        service_stop_button.setOnClickListener {
            mService!!.removeLocationUpdates()
            checkConnection!!.disableChecking()
            setButtonState(false)
        }

        setButtonState(Common.requestingLocationUpdates(this@MapsActivity))
        bindService(
            Intent(this@MapsActivity, TrackingService::class.java),
            mServiceConnection,
            Context.BIND_AUTO_CREATE
        )

        setButtonState(false)

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        getLastLocation()
        mMap.moveCamera(CameraUpdateFactory.zoomTo(14.0f))

        if (startedFromHistory) {
            if (destinationLocationMarker != null)
                destinationLocationMarker!!.remove()
            destinationLocationMarker =
                mMap.addMarker(MarkerOptions().position(destination!!).title("Cel"))
            mMap.moveCamera(CameraUpdateFactory.newLatLng(destination!!))
        }

        mMap.setOnMapClickListener {
            destination = it
            if (destinationLocationMarker != null)
                destinationLocationMarker!!.remove()
            destinationLocationMarker =
                mMap.addMarker(MarkerOptions().position(it).title("Cel"))
            mMap.moveCamera(CameraUpdateFactory.newLatLng(it))
        }
    }

    private fun buildAlertMessageNoGps() {
        val builder =
            AlertDialog.Builder(this)
        builder.setMessage("Usługa lokalizacji w twoim urządzeniu jest wyłączona. Czy chcesz ją włączyć?")
            .setCancelable(false)
            .setPositiveButton("TAK"
            ) { dialog, id -> startActivityForResult(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0) }
            .setNegativeButton("NIE"
            ) { dialog, id -> dialog.cancel() }
        val alert = builder.create()
        alert.show()
    }

    private fun setButtonState(boolean: Boolean) {
        if (boolean) {
            service_stop_button.isEnabled = true
            service_start_button.isEnabled = false
        } else {
            service_stop_button.isEnabled = false
            service_start_button.isEnabled = true
        }
    }

    private fun getLastLocation() {
        runWithPermissions(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            options = quickPermissionsOptions
        ) {
            try {
                fusedLocationProviderClient!!.lastLocation
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful && task.result != null) {
                            currentLocation = task.result
                            if (currentLocationMarker != null)
                                currentLocationMarker!!.remove()

                            if (currentLocation != null) {
                                val currentLocationLatLng =
                                    LatLng(currentLocation!!.latitude, currentLocation!!.longitude)
                                currentLocationMarker =
                                    mMap.addMarker(
                                        MarkerOptions().position(currentLocationLatLng)
                                            .title("Twoja lokalizacja!")
                                    )
                                mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocationLatLng))
                            }

                        } else
                            Toast.makeText(
                                this,
                                "Nie udało się pobrać ostatniej znanej lokalizacji. Włącz usługę lokalizacji. Gdy uruchomisz alarm nastąpi ponowna próba pobrania lokalizacji.",
                                Toast.LENGTH_LONG
                            ).show()

                        if (!Common.statusCheck(this)) buildAlertMessageNoGps()
                    }
            } catch (ex: SecurityException) {
                Log.e("catch", "" + ex.message)
            }
        }
    }

    private fun searchLocation() {
        val location = gmaps_search_address_txt.text.toString()
        if (getLocationByAddress(this, location) != null) {
            destination = getLocationByAddress(this, location)
            if (destinationLocationMarker != null)
                destinationLocationMarker!!.remove()
            destinationLocationMarker =
                mMap.addMarker(MarkerOptions().position(destination!!).title("Cel"))
            mMap.moveCamera(CameraUpdateFactory.newLatLng(destination))
        }
    }

    override fun onStart() {
        super.onStart()
        PreferenceManager.getDefaultSharedPreferences(this)
            .registerOnSharedPreferenceChangeListener(this)
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        PreferenceManager.getDefaultSharedPreferences(this)
            .unregisterOnSharedPreferenceChangeListener(this)
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onSharedPreferenceChanged(p0: SharedPreferences?, p1: String?) {
        if (p1.equals(Common.KEY_REQUEST_LOCATION_UPDATE))
            setButtonState(p0!!.getBoolean(Common.KEY_REQUEST_LOCATION_UPDATE, false))
    }

}
