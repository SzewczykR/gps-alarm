package com.example.budzikgps

import android.content.Context
import android.net.Uri

class Config(context: Context) {

    private val name = "gpsalarm.config"
    private val api = "API"
    private val ringtoneName = "RingtoneName"
    private val ringtoneUri = "RingtoneUri"
    private val radius = "Radius"
    private val frequency = "Frequency"
    private val gracePeriod = "Grace_period"
    private val history = "History"
    private val preference = context.getSharedPreferences(name, Context.MODE_PRIVATE)


    fun getHistory(): Set<String> {
        val defValue: Set<String> = setOf("")
        return preference.getStringSet(history, defValue)
    }

    fun setHistory(history: Set<String>) {
        val editor = preference.edit()
        editor.putStringSet(this.history, history)
        editor.apply()
    }

    fun setGracePeriod(gracePeriod: Int) {
        val editor = preference.edit()
        editor.putInt(this.gracePeriod, gracePeriod)
        editor.apply()
    }

    fun getGracePeriod(): Int {
        return preference.getInt(gracePeriod, 5)
    }

    fun getAPI(): String {
        return preference.getString(api, "gmaps")
    }

    fun setAPI(api: String) {
        val editor = preference.edit()
        editor.putString(this.api, api)
        editor.apply()
    }

    fun getRingtoneName(): String {
        return preference.getString(ringtoneName, "default")
    }

    fun setRingtoneName(name: String) {
        val editor = preference.edit()
        editor.putString(ringtoneName, name)
        editor.apply()
    }

    fun getRingtoneUri(): Uri {
        return Uri.parse(preference.getString(ringtoneUri, ""))
    }

    fun setRingtoneUri(uri: Uri) {
        val editor = preference.edit()
        editor.putString(ringtoneUri, uri.toString())
        editor.apply()
    }

    fun setRadius(radius: Int) {
        val editor = preference.edit()
        editor.putInt(this.radius, radius)
        editor.apply()
    }

    fun getRadius(): Int {
        return preference.getInt(radius, 500)
    }

    fun setFrequency(frequency: Int) {
        val editor = preference.edit()
        editor.putInt(this.frequency, frequency)
        editor.apply()
    }

    fun getFrequency(): Int {
        return preference.getInt(frequency, 20)
    }

}