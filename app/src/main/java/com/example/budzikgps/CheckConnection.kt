package com.example.budzikgps

import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.net.ConnectivityManager
import java.util.*


class CheckConnection(val context: Context) : TimerTask() {

    private val extraConnectionLost = "CONNECTION_LOST"
    private var cancelChecking = false


    fun disableChecking() {
        cancelChecking = true
    }

    override fun run() {
        if (NetworkUtils.isNetworkAvailable(context) && NetworkUtils.isLocationServiceAvailable(
                context
            )
        ) {
            if (cancelChecking) {
                cancelChecking = false
                cancel()
            }
        } else {
            val intent = Intent(context, AlarmService::class.java)
            intent.putExtra(extraConnectionLost, true)
            context.startService(intent)
            cancel()
        }
    }


    object NetworkUtils {
        fun isNetworkAvailable(context: Context): Boolean {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }

        fun isLocationServiceAvailable(context: Context): Boolean {
            val locationManager =
                context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            return locationManager.isProviderEnabled((LocationManager.GPS_PROVIDER))
        }
    }
}