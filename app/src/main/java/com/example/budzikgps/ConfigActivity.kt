package com.example.budzikgps

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_config.*
import kotlinx.android.synthetic.main.alert_login.view.*
import kotlinx.android.synthetic.main.alert_register.view.*
import xyz.aprildown.ultimateringtonepicker.RingtonePickerDialog
import xyz.aprildown.ultimateringtonepicker.RingtonePickerEntry
import xyz.aprildown.ultimateringtonepicker.RingtonePickerListener
import xyz.aprildown.ultimateringtonepicker.UltimateRingtonePicker

class ConfigActivity : AppCompatActivity(), RingtonePickerListener {

    private var isLoggedIn: Boolean = false
    private val radiusList: List<Int> =
        listOf(100, 200, 300, 500, 700, 1000, 1500, 2000, 5000, 10000)
    private val frequencyList: List<Int> = listOf(10, 15, 20, 30, 45, 60, 90, 180, 300, 600)
    private val gracePeriodList: List<Int> = listOf(0, 10, 30, 60, 120, 300, 600, 900, 1800, 3600)
    private val db = FirebaseFirestore.getInstance()
    val uid = FirebaseAuth.getInstance().uid ?: ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_config)
        val config = Config(this)
        isLoggedIn = checkAuth()

        setAuthButtons(isLoggedIn)
        checkAPI(config.getAPI())
        checkRingtone(config.getRingtoneName())
        setRadiusSeekbar(config.getRadius())
        setFrequencySeekbar(config.getFrequency())
        setGracePeriodSeekbar(config.getGracePeriod())
        if (isLoggedIn) {
            syncConfig()
        }
        config_login_btn.setOnClickListener {
            loginDialog()
        }

        config_register_btn.setOnClickListener {
            registerDialog()
        }

        config_logout_btn.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            setAuthButtons(false)
        }

        config_api_btn.setOnClickListener {
            apiDialog(config)
        }

        config_ringtone_btn.setOnClickListener {
            ringtonePicker()
        }

        config_graceperiod_seekbar.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                setGracePeriod(config, progress)
                checkGracePeriod(config.getGracePeriod())
                if (uid != "")
                    sendGracePeriodToFB(gracePeriodList[progress])
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

        })

        config_frequency_seekbar.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                setFrequency(config, progress)
                checkFrequency(config.getFrequency())

                if (uid != "")
                    sendFrequencyToFB(frequencyList[progress])
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }


        })

        config_radius_seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                setRadius(config, progress)
                checkRadius(config.getRadius())

                if (uid != "") sendRadiusToFb(radiusList[progress])
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }

        })

    }

    private fun sendGracePeriodToFB(gracePeriod: Int) {
        db.collection("settings").document(uid).update("grace_period", gracePeriod)
    }

    private fun sendFrequencyToFB(frequency: Int) {
        db.collection("settings").document(uid).update("frequency", frequency)
    }

    private fun sendRadiusToFb(radius: Int) {
        db.collection("settings").document(uid).update("radius", radius)
    }

    private fun sendApiToFb(api: String) {
        db.collection("settings").document(uid).update("api", api)
    }

    private fun sendRingtoneNameToFb(ringtoneName: String) {
        db.collection("settings").document(uid).update("ringtone_name", ringtoneName)
    }

    private fun sendRingtoneUriToFb(ringtoneUri: String) {
        db.collection("settings").document(uid).update("ringtone_uri", ringtoneUri)
    }

    private fun syncConfig() {
        val settingsRef = db.collection("settings")
        settingsRef.document(uid).get()
            .addOnSuccessListener {

                val config = Config(this)
                if (it.get("api").toString() != "null") {
                    config.setAPI(it.get("api").toString())
                }
                if (it.get("frequency").toString() != "null") {
                    config.setFrequency(it.get("frequency").toString().toInt())
                }
                if (it.get("grace_period").toString() != "null") {
                    config.setGracePeriod(it.get("grace_period").toString().toInt())
                }
                if (it.get("radius").toString() != "null") {
                    config.setRadius(it.get("radius").toString().toInt())
                }
                if (it.get("ringtone_uri").toString() != "null") {
                    config.setRingtoneUri(it.get("ringtone_uri").toString().toUri())
                }
                if (it.get("ringtone_name").toString() != "null") {
                    config.setRingtoneName(it.get("ringtone_name").toString())
                }
            }
            .addOnFailureListener {
                Toast.makeText(
                    this,
                    "Nie udało się pobrać ustawień z bazy danych.",
                    Toast.LENGTH_LONG
                ).show()
            }


    }

    private fun checkGracePeriod(gracePeriod: Int) {

        if (gracePeriod == 0) {
            config_graceperiodvalue_txt.text = "Wyłączone"
        } else {
            config_graceperiodvalue_txt.text = gracePeriod.toString() + "s"
        }


    }

    private fun setGracePeriod(config: Config, progress: Int) {
        config.setGracePeriod(gracePeriodList[progress])
    }

    private fun setGracePeriodSeekbar(gracePeriod: Int) {
        config_graceperiod_seekbar.progress = gracePeriodList.indexOf(gracePeriod)

        if (gracePeriod == 0) {
            config_graceperiodvalue_txt.text = "Wyłączone"
        } else {
            config_graceperiodvalue_txt.text = gracePeriod.toString() + "s"
        }


    }

    private fun setFrequencySeekbar(frequency: Int) {
        config_frequency_seekbar.progress = frequencyList.indexOf(frequency)
        config_frequencyvalue_txt.text = frequency.toString() + "s"
    }

    private fun setFrequency(config: Config, progress: Int) {
        config.setFrequency(frequencyList[progress])
    }

    private fun checkFrequency(frequency: Int) {
        config_frequencyvalue_txt.text = frequency.toString() + "s"
    }

    private fun setRadiusSeekbar(radius: Int) {
        config_radius_seekbar.progress = radiusList.indexOf(radius)
        config_radiusvalue_txt.text = radius.toString() + "m"
    }

    private fun setRadius(config: Config, progress: Int) {
        config.setRadius(radiusList[progress])
    }

    private fun checkRadius(radius: Int) {
        config_radiusvalue_txt.text = radius.toString() + "m"
    }

    private fun checkRingtone(name: String) {
        if (name == "" || name == "default") {
            config_ringtone_txt.text = "Domyślny dźwięk alarmu"
        } else {
            config_ringtone_txt.text = name
        }

    }

    private fun ringtonePicker() {
        RingtonePickerDialog.createInstance(
            UltimateRingtonePicker.Settings(
                showCustomRingtone = false,
                systemRingtoneTypes = listOf(UltimateRingtonePicker.Settings.SYSTEM_RINGTONE_TYPE_ALARM),
                showSilent = false
            ),
            "Wybierz dzwonek"
        ).show(supportFragmentManager, null)
    }

    @SuppressLint("InflateParams")
    private fun loginDialog() {
        val customView = layoutInflater.inflate(R.layout.alert_login, null)
        val builder = AlertDialog.Builder(this)
            .setView(customView)
        builder.setTitle("Logowanie")
        builder.setPositiveButton("Zaloguj") { dialog, which ->
            val email = customView.alert_login_email.text.toString()
            val password = customView.alert_login_password.text.toString()
            logIn(email, password)
        }
        builder.setNegativeButton("Anuluj") { dialog, which -> }
        builder.show()
    }

    @SuppressLint("InflateParams")
    private fun registerDialog() {
        val customView = layoutInflater.inflate(R.layout.alert_register, null)
        val builder = AlertDialog.Builder(this)
            .setView(customView)
        builder.setTitle("Rejestracja")
        builder.setPositiveButton("Zarejestruj") { dialog, which ->
            val email = customView.alert_register_email.text.toString()
            val password1 = customView.alert_register_password1.text.toString()
            val password2 = customView.alert_register_password2.text.toString()
            if (password1 != password2) {
                Toast.makeText(this, "Hasła nie pasują do siebie!", Toast.LENGTH_LONG).show()
            } else {
                register(email, password1)
            }
        }
        builder.setNegativeButton("Anuluj") { dialog, which ->

        }
        builder.show()
    }

    private fun checkAuth(): Boolean {
        val uid = FirebaseAuth.getInstance().uid
        return uid != null
    }

    private fun setAuthButtons(isLoggedIn: Boolean) {
        if (isLoggedIn) {
            config_register_btn.isEnabled = false
            config_register_btn.isClickable = false
            config_login_btn.isEnabled = false
            config_login_btn.isClickable = false
            config_logout_btn.isEnabled = true
            config_logout_btn.isClickable = true
        } else {
            config_register_btn.isEnabled = true
            config_register_btn.isClickable = true
            config_login_btn.isEnabled = true
            config_login_btn.isClickable = true
            config_logout_btn.isEnabled = false
            config_logout_btn.isClickable = false
        }
    }

    private fun logIn(email: String, password: String) {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                Toast.makeText(this, "Jesteś zalogowany!", Toast.LENGTH_LONG).show()
                setAuthButtons(true)
            }
            .addOnFailureListener { exception ->
                Toast.makeText(
                    this,
                    "Nie udało się zalogować. Błąd:\n${exception.message}",
                    Toast.LENGTH_LONG
                ).show()
            }
    }

    private fun register(email: String, password: String) {
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                Toast.makeText(
                    this,
                    "Twoje konto zostało poprawnie zarejestrowane.\nZostałeś automatycznie zalogowany.",
                    Toast.LENGTH_LONG
                ).show()
                setAuthButtons(true)
            }
            .addOnFailureListener { exception ->
                Toast.makeText(
                    this,
                    "Nie udało się utworzyć konta. Błąd:\n${exception.message}",
                    Toast.LENGTH_LONG
                ).show()
            }
    }

    private fun checkAPI(api: String) {
        if (api == "mapbox") {
            config_api_txt.text = "MapBox"
        } else {
            config_api_txt.text = "Google Maps"
        }
    }

    private fun apiDialog(config: Config) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Wybierz mapę")
        builder.setPositiveButton("Google Maps") { dialog, which ->
            config.setAPI("gmaps")
            checkAPI(config.getAPI())
            if (uid != "")
                sendApiToFb("gmaps")
        }
        builder.setNegativeButton("MapBox") { dialog, which ->
            config.setAPI("mapbox")
            checkAPI(config.getAPI())
            if (uid != "")
                sendApiToFb("mapbox")
        }
        builder.show()
    }

    override fun onRingtonePicked(ringtones: List<RingtonePickerEntry>) {
        val context = applicationContext
        val config = Config(context)
        config.setRingtoneName(ringtones[0].name)
        config.setRingtoneUri(ringtones[0].uri)

        if (uid != "") {
            sendRingtoneUriToFb(ringtones[0].uri.toString())
            sendRingtoneNameToFb(ringtones[0].name)
        }
        checkRingtone(config.getRingtoneName())
    }

}
