package com.example.budzikgps

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.Ringtone
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat

class AlarmService : Service() {

    private var isRunning: Boolean = false
    private val notificationId = 1
    private var alarmUri: Uri? = null
    private var ringtoneManager: Ringtone? = null

    private val title = "Dotarłeś do celu!"
    private val notificationAction = "Wyłącz alarm"
    private val title2 = "NIE MOŻNA POBRAĆ LOKALIZACJI"
    private val content2 = "UTRACONO POŁĄCZENIE LUB USŁUGA\nLOKALIZACJI ZOSTAŁA WYŁĄCZONA!"
    private val notificationAction2 = "Zamknij notyfikację"
    private val extraFromAlarmService = "EXTRA_STARTED_FROM_ALARM_SERVICE"
    private val extraFromMaps = "EXTRA_STARTED_FROM_MAPS"
    private val extraConnectionLost = "CONNECTION_LOST"

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String {
        val chan = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        chan.setSound(null, null)
        val service = getSystemService((Context.NOTIFICATION_SERVICE)) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    private val notificationConnectionLost: Notification
        get() {
            val channelId = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel("gpsalarm_alarm_service", "GpsAlarm Alarm Service")
            } else {
                ""
            }
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra(extraFromAlarmService, true)
            val activityPendingIntent =
                PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            val builder = NotificationCompat.Builder(this)
                .addAction(
                    R.drawable.mapbox_logo_icon,
                    notificationAction2,
                    activityPendingIntent
                )
                .setContentTitle(title2)
                .setContentText(content2)
                .setOngoing(false)
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setWhen(System.currentTimeMillis())
                .setSound(null)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                builder.setChannelId(channelId)
            return builder.build()
        }

    private val notification: Notification
        get() {
            val channelId = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel("gpsalarm_alarm_service", "GpsAlarm Alarm Service")
            } else {
                ""
            }
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra(extraFromAlarmService, true)
            val activityPendingIntent =
                PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            val builder = NotificationCompat.Builder(this)
                .addAction(
                    R.drawable.mapbox_marker_icon_default,
                    notificationAction,
                    activityPendingIntent
                )
                .setContentTitle(title)
                .setOngoing(false)
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setWhen(System.currentTimeMillis())
                .setSound(null)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                builder.setChannelId(channelId)
            return builder.build()
        }


    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val startedFromMaps = intent?.getBooleanExtra(extraFromMaps, false)
        val startedFromConnectionLost = intent?.getBooleanExtra(extraConnectionLost, false)
        when {
            startedFromMaps!! -> {
                ringtoneManager?.stop()
                stopForeground(true)
                stopSelf()
                val intent2 = Intent(this, TrackingService::class.java)
                intent2.putExtra(extraFromMaps, true)
                startService(intent2)
            }
            startedFromConnectionLost!! -> {
                startForeground(notificationId, notificationConnectionLost)
                val config = Config(this)
                alarmUri = config.getRingtoneUri()
                ringtoneManager = RingtoneManager.getRingtone(this, alarmUri)
                ringtoneManager?.play()
            }
            else -> {
                startForeground(notificationId, notification)
                val config = Config(this)
                alarmUri = config.getRingtoneUri()
                ringtoneManager = RingtoneManager.getRingtone(this, alarmUri)
                ringtoneManager?.play()
            }
        }
        return START_NOT_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        this.isRunning = false
    }
}