package com.example.budzikgps

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.activity_history.*
import kotlinx.android.synthetic.main.alert_history.view.*
import kotlinx.android.synthetic.main.item.view.*


class HistoryActivity : AppCompatActivity() {

    private var historyList: MutableList<HistoryEntry> = mutableListOf()
    private val db = FirebaseFirestore.getInstance()
    private val uid = FirebaseAuth.getInstance().uid ?: ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        if (uid != "")
            fetchHistory()
        else getHistory()

    }

    @SuppressLint("InflateParams")
    private fun getHistory() {
        val adapter = GroupAdapter<GroupieViewHolder>()
        val config = Config(this)

        val api = config.getAPI()
        var intent = Intent()
        intent = if (api == "mapbox") {
            Intent(this, MapboxActivity::class.java)
        } else {
            Intent(this, MapsActivity::class.java)
        }

        historyList.clear()
        adapter.clear()
        if (config.getHistory().toString() == "" || config.getHistory().toString() == "[]") {
            history_text.alpha = 1f
        } else {
            history_text.alpha = 0f
            val history = config.getHistory()

            adapter.setOnItemLongClickListener { item, view ->

                val historyItem = item as HistoryItem
                val customView = layoutInflater.inflate(R.layout.alert_history, null)
                val builder = AlertDialog.Builder(this)
                    .setView(customView)
                val alertDialog = builder.show()
                customView.historyalert_cancel.setOnClickListener {
                    alertDialog.dismiss()
                }
                customView.historyalert_delete.setOnClickListener {
                    alertDialog.dismiss()
                    historyList.removeAt(item.index)
                    val temp: MutableList<String> = mutableListOf()
                    historyList.forEach { temp.add(it.toString()) }
                    config.setHistory(temp.toSet())
                    adapter.remove(item)

                    val groupCount = adapter.groupCount
                    val tempAdapter = GroupAdapter<GroupieViewHolder>()
                    for (i in 0 until groupCount) {
                        val historyItem = adapter.getItem(i) as HistoryItem
                        historyItem.index = i
                        tempAdapter.add(historyItem)
                    }


                    if (adapter.itemCount == 0) history_text.alpha = 1f

                    adapter.notifyDataSetChanged()
                    updateHistory()
                }
                customView.historyalert_favorite.setOnClickListener {
                    alertDialog.dismiss()

                    if (historyList[item.index].favorite) {
                        historyList[item.index].favorite = false
                        item.favorite = false
                    } else {
                        historyList[item.index].favorite = true
                        item.favorite = true
                    }
                    val temp: MutableList<String> = mutableListOf()
                    historyList.forEach { temp.add(it.toString()) }
                    config.setHistory(temp.toSet())
                    adapter.notifyDataSetChanged()
                    updateHistory()
                }
                return@setOnItemLongClickListener true
            }

            adapter.setOnItemClickListener { item, view ->
                val historyItem = item as HistoryItem
                intent.putExtra("STARTED_FROM_HISTORY", true)
                intent.putExtra("LATITUDE", item.latitude)
                intent.putExtra("LONGITUDE", item.longitude)
                startActivity(intent)
            }

            history.forEachIndexed { index, it ->

                val address = it.substringAfter("address=").substringBefore(", favorite")
                val favoriteStr = it.substringAfter("favorite=").substringBefore(", timestamp")
                val favorite = favoriteStr == "true"
                val timestamp = it.substringAfter("timestamp=").substringBefore(")")
                val latitude = it.substringAfter(": (").substringBefore(",")
                val longitude = it.substringAfter(",").substringBefore(")")
                val location = LatLng(latitude.toDouble(), longitude.toDouble())

                historyList.add(HistoryEntry(location, address, favorite, timestamp))
            }
            sortHistory()
            historyList.forEachIndexed { index, it ->
                adapter.add(
                    HistoryItem(
                        it.location.latitude,
                        it.location.longitude,
                        index,
                        it.address,
                        it.timestamp,
                        it.favorite
                    )
                )
            }

            historyList.forEachIndexed { index, historyEntry ->
            }
            history_recyclerview.adapter = adapter
        }
    }

    private fun sortHistory() {
        historyList.sortBy { it.address }
        historyList.reverse()
        historyList.sortBy { it.favorite }
        historyList.reverse()
    }

    private fun updateHistory() {

        var sizeFB = 0
        val sizeSP = historyList.size

        if (sizeSP == 0 && uid!="") {

            db.collection("history").document(uid).collection("history").document("0").delete()
        }


        historyList.forEachIndexed { index, historyEntry ->


            if(uid!=""){

            db.collection("history").document(uid).collection("history").get()
                .addOnSuccessListener {
                    sizeFB = it.size()


                    if (sizeFB > sizeSP) {
                        for (i in sizeSP until sizeFB) {
                            db.collection("history").document(uid).collection("history")
                                .document(i.toString())
                                .delete()
                        }
                    }
                    val entry = hashMapOf(
                        "latitude" to historyEntry.location.latitude,
                        "longitude" to historyEntry.location.longitude,
                        "address" to historyEntry.address,
                        "favorite" to historyEntry.favorite.toString(),
                        "timestamp" to historyEntry.timestamp
                    )
                    db.collection("history").document(uid).collection("history")
                        .document(index.toString())
                        .set(entry)
                }
            }
        }
    }

    private fun fetchHistory() {
        getHistory()
        val config = Config(this)
        db.collection("history").document(uid).collection("history").get()
            .addOnSuccessListener { it ->
                if (!it.isEmpty) {
                    historyList.clear()
                    it.documents.forEach { it ->
                        val location = LatLng(
                            it.get("latitude").toString().toDouble(),
                            it.get("longitude").toString().toDouble()
                        )
                        val address = it.get("address").toString()
                        val favoriteStr = it.get("favorite").toString()
                        val favorite = favoriteStr == "true"
                        val timestamp = it.get("timestamp").toString()
                        val historyEntry = HistoryEntry(location, address, favorite, timestamp)
                        historyList.add(historyEntry)
                        val temp: MutableList<String> = mutableListOf()
                        historyList.forEach { temp.add(it.toString()) }
                        config.setHistory(temp.toSet())
                        getHistory()
                    }
                } else {
                    getHistory()
                }
            }
    }
}

class HistoryItem(
    val latitude: Double,
    val longitude: Double,
    var index: Int,
    private val address: String,
    private val timestamp: String,
    var favorite: Boolean
) : Item<GroupieViewHolder>() {

    override fun getLayout(): Int {
        return R.layout.item
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.item_text.text = "$address, $timestamp"
        if (favorite) {
            viewHolder.itemView.item_star_img.setImageResource(android.R.drawable.btn_star_big_on)
        } else {
            viewHolder.itemView.item_star_img.setImageResource(android.R.drawable.btn_star_big_off)
        }
    }
}
