package com.example.budzikgps

import com.google.android.gms.maps.model.LatLng
import java.io.Serializable

data class HistoryEntry(
    val location: LatLng,
    val address: String,
    var favorite: Boolean,
    val timestamp: String
) : Serializable