package com.example.budzikgps

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var api: String

    override fun onStart() {
        super.onStart()
        val config = Config(this)
        api = config.getAPI()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val notif = intent.getBooleanExtra("EXTRA_STARTED_FROM_ALARM_SERVICE", false)
        if (notif) {
            val intent = Intent(this, AlarmService::class.java)
            intent.putExtra("EXTRA_STARTED_FROM_MAPS", true)
            startService(intent)
        }
        val notif2 = intent.getBooleanExtra("EXTRA_STARTED_FROM_TRACKING_SERVICE", false)
        if (notif2) {
            val intent = Intent(this, TrackingService::class.java)
            intent.putExtra("EXTRA_STARTED_FROM_MAPS", true)
            startService(intent)
        }

        val config = Config(this)
        api = config.getAPI()
        main_config_btn.setOnClickListener {
            val intent = Intent(this, ConfigActivity::class.java)
            startActivity(intent)
        }

        main_maps_btn.setOnClickListener {
            var intent: Intent?
            intent = if (api == "mapbox") {
                Intent(this, MapboxActivity::class.java)
            } else {
                Intent(this, MapsActivity::class.java)
            }
            startActivity(intent)
        }

        main_history_btn.setOnClickListener {
            val intent = Intent(this, HistoryActivity::class.java)
            startActivity(intent)
        }

    }
}
